package com.identyum.demo.utils;

import android.util.Log;

import androidx.annotation.NonNull;

import com.identyum.demo.adapters.DocumentsAdapter;
import com.identyum.demo.adapters.model.Document;
import com.identyum.demo.oauth.OAuth2Service;
import com.identyum.demo.rest.ApiInterface;
import com.identyum.demo.rest.model.ApiImageResponse;

import lombok.val;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Utils {

  private Utils() {
  }

  public static final int TYPE_FRONT_IMAGE = 0;
  public static final int TYPE_BACK_IMAGE = 1;
  public static final int TYPE_SIGNATURE_IMAGE = 2;

  public static String generateStackTrace(Exception ex) {
    return
        "*** EXCEPTION ***" +
            "\n" +
            "Message: " +
            ex.getMessage() +
            "\n" +
            Log.getStackTraceString(ex) +
            "\n";
  }

  public static void downloadDocumentImageAsync(
      ApiInterface apiService,
      DocumentsAdapter adapter,
      Document document,
      String token,
      String imageId,
      int imageType,
      int itemPosition
  ) {
    val call = apiService.getImage(imageId, "Bearer " + token, OAuth2Service.SECRET_KEY);
    call.enqueue(new Callback<ApiImageResponse>() {

      @Override
      public void onResponse(@NonNull Call<ApiImageResponse> call, @NonNull Response<ApiImageResponse> response) {
        val body = response.body();
        if (body != null) {

          val base64Image = body.getBase64Value();
          if (base64Image == null || base64Image.trim().length() == 0) {
            return;
          }

          switch (imageType) {
            case TYPE_FRONT_IMAGE:
              document.getFrontImage().setValue(base64Image);
              adapter.notifyItemChanged(itemPosition, document);
              break;
            case TYPE_BACK_IMAGE:
              document.getBackImage().setValue(base64Image);
              adapter.notifyItemChanged(itemPosition, document);
              break;
            case TYPE_SIGNATURE_IMAGE:
              document.getSignatureImage().setValue(base64Image);
              adapter.notifyItemChanged(itemPosition, document);
          }
        }
      }

      @Override
      public void onFailure(@NonNull Call<ApiImageResponse> call, @NonNull Throwable t) {
        Log.e(this.getClass().toString(), "Couldn't retrieve ApiImageResponse");
      }
    });
  }

}
