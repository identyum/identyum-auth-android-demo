package com.identyum.demo.adapters.model;

import com.identyum.demo.rest.model.ApiContact;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Value;

@Value
public class ProfileContact {

  String type;

  String value;

  public static ProfileContact from(ApiContact contact) {
    return new ProfileContact(
        contact.getType(),
        contact.getValue()
    );
  }

  public static List<ProfileContact> from(List<ApiContact> contacts) {
    if (contacts == null) {
      return Collections.emptyList();
    }
    return contacts.stream().map(ProfileContact::from).collect(Collectors.toList());
  }
}