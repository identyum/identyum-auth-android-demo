package com.identyum.demo.adapters.model;

import com.identyum.demo.rest.model.ApiAddress;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@lombok.Value
public class Address {

  Value value;

  public static Address from(ApiAddress address) {
    return new Address(
        Value.from(address.getValue())
    );
  }

  public static List<Address> from(List<ApiAddress> addresses) {
    if (addresses == null) {
      return Collections.emptyList();
    }
    return addresses.stream().map(Address::from).collect(Collectors.toList());
  }
}
