package com.identyum.demo.adapters.model;

import com.identyum.demo.rest.model.ApiImageResponse;
import com.identyum.demo.rest.model.ApiValue;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Value {

  String id;

  String type;

  String value;

  public static Value from(ApiValue value) {
    if (value == null) {
      return null;
    }
    return new Value()
        .setId(value.getId())
        .setType(value.getType())
        .setValue(value.getValue());
  }

  public static Value from(ApiImageResponse response) {
    if (response == null) {
      return null;
    }
    return new Value()
        .setId(response.getId())
        .setValue(response.getBase64Value());
  }

}

