package com.identyum.demo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.identyum.demo.R;
import com.identyum.demo.adapters.model.Address;

import java.util.List;

import lombok.val;

public class AddressesAdapter extends RecyclerView.Adapter<AddressesAdapter.myViewHolder> {

  private final List<Address> addresses;

  public AddressesAdapter(List<Address> addresses) {
    this.addresses = addresses;
  }

  public static class myViewHolder extends RecyclerView.ViewHolder {
    private final TextView addressTypeView;
    private final TextView addressValueView;

    public myViewHolder(View itemView) {
      super(itemView);
      addressTypeView = itemView.findViewById(R.id.address_type_view);
      addressValueView = itemView.findViewById(R.id.address_value_view);
    }
  }

  @NonNull
  @Override
  public AddressesAdapter.myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    val listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_address_layout, parent, false);
    return new AddressesAdapter.myViewHolder(listItem);
  }

  @Override
  public void onBindViewHolder(@NonNull final AddressesAdapter.myViewHolder holder, int position) {
    val address = addresses.get(position);
    val value = address.getValue();
    if (value != null) {
      holder.addressTypeView.setText(value.getType() == null ? "" : value.getType());
      holder.addressValueView.setText(value.getValue() == null ? "" : value.getValue());
    }
  }

  @Override
  public int getItemCount() {
    return addresses.size();
  }

}

