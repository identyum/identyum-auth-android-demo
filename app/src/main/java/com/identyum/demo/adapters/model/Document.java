package com.identyum.demo.adapters.model;

import com.identyum.demo.rest.model.ApiDocument;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@lombok.Value
public class Document {

  Value frontImage;

  Value backImage;

  Value signatureImage;

  Value dateOfIssue;

  Value dateOfExpiry;

  Value type;

  Value number;

  Value issuingCountryCode;

  Value issuedBy;

  public static Document from(ApiDocument document) {
    return new Document(
        Value.from(document.getFrontImage()),
        Value.from(document.getBackImage()),
        Value.from(document.getSignatureImage()),
        Value.from(document.getDateOfIssue()),
        Value.from(document.getDateOfExpiry()),
        Value.from(document.getType()),
        Value.from(document.getNumber()),
        Value.from(document.getIssuingCountryCode()),
        Value.from(document.getIssuedBy())
    );
  }

  public static List<Document> from(List<ApiDocument> documents) {
    if (documents == null) {
      return Collections.emptyList();
    }
    return documents.stream().map(Document::from).collect(Collectors.toList());
  }
}
