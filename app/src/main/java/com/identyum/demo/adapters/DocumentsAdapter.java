package com.identyum.demo.adapters;

import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.identyum.demo.R;
import com.identyum.demo.adapters.model.Document;

import java.util.List;

import lombok.val;

public class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.myViewHolder> {

  private final List<Document> documents;

  public DocumentsAdapter(List<Document> documents) {
    this.documents = documents;
  }

  public static class myViewHolder extends RecyclerView.ViewHolder {

    private final ImageView frontImageView;
    private final ImageView backImageView;
    private final ImageView signatureImageView;
    private final TextView dateOfIssueView;
    private final TextView dateOfExpiryView;
    private final TextView typeView;
    private final TextView numberView;
    private final TextView issuingCountryCodeView;
    private final TextView issuedByView;

    public myViewHolder(View itemView) {
      super(itemView);
      frontImageView = itemView.findViewById(R.id.document_front_image_view);
      backImageView = itemView.findViewById(R.id.document_back_image_view);
      signatureImageView = itemView.findViewById(R.id.document_signature_image_view);
      dateOfIssueView = itemView.findViewById(R.id.document_date_of_issue_view);
      dateOfExpiryView = itemView.findViewById(R.id.document_date_of_expiry_view);
      typeView = itemView.findViewById(R.id.document_type_view);
      numberView = itemView.findViewById(R.id.document_number_view);
      issuingCountryCodeView = itemView.findViewById(R.id.document_issuing_country_code_view);
      issuedByView = itemView.findViewById(R.id.document_issued_by_view);
    }
  }

  @NonNull
  @Override
  public DocumentsAdapter.myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    val listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_document_layout, parent, false);
    return new DocumentsAdapter.myViewHolder(listItem);
  }

  @Override
  public void onBindViewHolder(final DocumentsAdapter.myViewHolder holder, int position) {
    val document = documents.get(position);

    holder.dateOfIssueView.setText(document.getDateOfIssue() == null ? "" : document.getDateOfIssue().getValue());
    holder.dateOfExpiryView.setText(document.getDateOfExpiry() == null ? "" : document.getDateOfExpiry().getValue());
    holder.typeView.setText(document.getType() == null ? "" : document.getType().getValue());
    holder.numberView.setText(document.getNumber() == null ? "" : document.getNumber().getValue());
    holder.issuingCountryCodeView.setText(document.getIssuingCountryCode() == null ? "" : document.getIssuingCountryCode().getValue());
    holder.issuedByView.setText(document.getIssuedBy() == null ? "" : document.getIssuedBy().getValue());

    val frontImage = document.getFrontImage();
    val backImage = document.getBackImage();
    val signatureImage = document.getSignatureImage();

    if (frontImage != null && frontImage.getValue() != null) {
      val decodedImageBytes = Base64.decode(frontImage.getValue(), Base64.DEFAULT);
      if (decodedImageBytes != null && decodedImageBytes.length > 0) {
        holder.frontImageView.setImageBitmap(
            BitmapFactory.decodeByteArray(
                decodedImageBytes,
                0,
                decodedImageBytes.length
            )
        );
      }
    }

    if (backImage != null && backImage.getValue() != null) {
      val decodedImageBytes = Base64.decode(backImage.getValue(), Base64.DEFAULT);
      if (decodedImageBytes != null && decodedImageBytes.length > 0) {
        holder.backImageView.setImageBitmap(
            BitmapFactory.decodeByteArray(
                decodedImageBytes,
                0,
                decodedImageBytes.length
            )
        );
      }
    }

    if (signatureImage != null && signatureImage.getValue() != null) {
      val decodedImageBytes = Base64.decode(signatureImage.getValue(), Base64.DEFAULT);
      if (decodedImageBytes != null && decodedImageBytes.length > 0) {
        holder.signatureImageView.setImageBitmap(
            BitmapFactory.decodeByteArray(
                decodedImageBytes,
                0,
                decodedImageBytes.length
            )
        );
      }
    }
  }

  @Override
  public int getItemCount() {
    return documents.size();
  }

}

