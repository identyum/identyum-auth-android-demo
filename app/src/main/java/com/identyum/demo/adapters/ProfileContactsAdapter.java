package com.identyum.demo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.identyum.demo.R;
import com.identyum.demo.adapters.model.ProfileContact;

import java.util.List;

import lombok.val;

public class ProfileContactsAdapter extends RecyclerView.Adapter<ProfileContactsAdapter.myViewHolder> {

  private final List<ProfileContact> contacts;

  public ProfileContactsAdapter(List<ProfileContact> contacts) {
    this.contacts = contacts;
  }

  public static class myViewHolder extends RecyclerView.ViewHolder {

    private final TextView contactTypeView;
    private final TextView contactValueView;

    public myViewHolder(View itemView) {
      super(itemView);
      contactTypeView = itemView.findViewById(R.id.profile_contact_type_view);
      contactValueView = itemView.findViewById(R.id.profile_contact_value_view);
    }
  }

  @NonNull
  @Override
  public ProfileContactsAdapter.myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    val listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_profile_contact_layout, parent, false);
    return new myViewHolder(listItem);
  }

  @Override
  public void onBindViewHolder(final ProfileContactsAdapter.myViewHolder holder, int position) {
    val contact = contacts.get(position);
    holder.contactTypeView.setText(contact.getType() == null ? "" : contact.getType());
    holder.contactValueView.setText(contact.getValue() == null ? "" : contact.getValue());
  }

  @Override
  public int getItemCount() {
    return contacts.size();
  }

}
