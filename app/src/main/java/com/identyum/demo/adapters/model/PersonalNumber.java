package com.identyum.demo.adapters.model;

import com.identyum.demo.rest.model.ApiPersonalNumber;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@lombok.Value
public class PersonalNumber {

  Value value;

  public static PersonalNumber from(ApiPersonalNumber personalNumber) {
    return new PersonalNumber(
        Value.from(personalNumber.getValue())
    );
  }

  public static List<PersonalNumber> from(List<ApiPersonalNumber> personalNumbers) {
    if (personalNumbers == null) {
      return Collections.emptyList();
    }
    return personalNumbers.stream().map(PersonalNumber::from).collect(Collectors.toList());
  }
}
