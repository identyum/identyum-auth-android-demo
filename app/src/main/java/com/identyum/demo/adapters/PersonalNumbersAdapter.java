package com.identyum.demo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.identyum.demo.R;
import com.identyum.demo.adapters.model.PersonalNumber;

import java.util.List;

import lombok.val;

public class PersonalNumbersAdapter extends RecyclerView.Adapter<PersonalNumbersAdapter.myViewHolder> {

  private final List<PersonalNumber> personalNumbers;

  public PersonalNumbersAdapter(List<PersonalNumber> personalNumbers) {
    this.personalNumbers = personalNumbers;
  }

  public static class myViewHolder extends RecyclerView.ViewHolder {

    private final TextView personalNumberTypeView;
    private final TextView personalNumberValueView;

    public myViewHolder(View itemView) {
      super(itemView);
      personalNumberTypeView = itemView.findViewById(R.id.personal_number_type_view);
      personalNumberValueView = itemView.findViewById(R.id.personal_number_value_view);
    }
  }

  @NonNull
  @Override
  public PersonalNumbersAdapter.myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    val listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_personal_number_layout, parent, false);
    return new PersonalNumbersAdapter.myViewHolder(listItem);
  }

  @Override
  public void onBindViewHolder(@NonNull final PersonalNumbersAdapter.myViewHolder holder, int position) {
    val personalNumber = personalNumbers.get(position);
    val value = personalNumber.getValue();
    if (value != null) {
      holder.personalNumberTypeView.setText(value.getType() == null ? "" : value.getType());
      holder.personalNumberValueView.setText(value.getValue() == null ? "" : value.getValue());
    }
  }

  @Override
  public int getItemCount() {
    return personalNumbers.size();
  }

}
