package com.identyum.demo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;
import com.identyum.appauth.AuthorizationException;
import com.identyum.appauth.AuthorizationResponse;
import com.identyum.appauth.AuthorizationService;
import com.identyum.appauth.NoClientAuthentication;
import com.identyum.appauth.TokenResponse;
import com.identyum.demo.oauth.OAuth2Service;
import com.identyum.demo.utils.Utils;

import lombok.val;

public class CoreDataHandlingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_core_data_handling);

    /* If authorization has already been performed just construct GUI,
     * otherwise first exchange authorization code for access token.
     */
    if (OAuth2Service.authState != null && OAuth2Service.authState.isAuthorized()) {
      constructGUI(savedInstanceState);
      return;
    }

    /* ----- OAUTH2 TOKEN RETRIEVAL ----- */

    val authorizationResponse = AuthorizationResponse.fromIntent(getIntent());
    if (authorizationResponse == null) {

      val exception = AuthorizationException.fromIntent(getIntent());
      Log.e(this.getClass().toString(), "Authorization code not received");
      if (exception != null) {
        Log.e(this.getClass().toString(), Utils.generateStackTrace(exception));
      }

      /* return to main/login activity via intent */
      OAuth2Service.logout(this);
      return;
    }

    Log.i(this.getClass().toString(), "Authorization code received");
    Log.i(this.getClass().toString(), "Exchanging authorization code for token...");

    val context = this;
    new AuthorizationService(this).performTokenRequest(
        authorizationResponse.createTokenExchangeRequest(),
        NoClientAuthentication.INSTANCE,
        new AuthorizationService.TokenResponseCallback() {
          @Override
          public void onTokenRequestCompleted(TokenResponse resp, AuthorizationException ex) {
            if (resp == null) {

              Log.e(this.getClass().toString(), "Token not received");
              Log.e(this.getClass().toString(), Utils.generateStackTrace(ex));

              /* Return to main/login activity via intent */
              OAuth2Service.logout(context);

            } else {

              Log.i(this.getClass().toString(), "Token received");
              OAuth2Service.authState.update(resp, ex);

              constructGUI(savedInstanceState);
            }
          }
        });
  }

  @Override
  public void onBackPressed() {
    val drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    }
  }

  @SuppressLint("NonConstantResourceId")
  private void displaySelectedScreen(int itemId) {
    Fragment fragment = null;

    if (itemId == R.id.profile_information_menu) {
      fragment = new FragmentProfileInformation();
    } else if (itemId == R.id.personal_data_menu) {
      fragment = new FragmentPersonalData();
    } else if (itemId == R.id.liveness_image_menu) {
      fragment = new FragmentLivenessImage();
    } else {
      OAuth2Service.logout(this);
    }

    if (fragment != null) {
      val fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(R.id.content_frame, fragment);
      fragmentTransaction.commit();
    }

    val drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
  }

  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    displaySelectedScreen(item.getItemId());
    return true;
  }

  @Override
  protected void onPause() {
    super.onPause();
    OAuth2Service.saveState(this);
  }

  private void constructGUI(Bundle savedInstanceState) {

    val toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    val drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    val toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    toggle.syncState();

    val navigationView = (NavigationView) findViewById(R.id.navigation_view_id);
    navigationView.setNavigationItemSelectedListener(this);

    if (savedInstanceState == null) {
      displaySelectedScreen(R.id.profile_information_menu);
    }
  }
}

