package com.identyum.demo;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.identyum.appauth.AuthState;
import com.identyum.appauth.AuthorizationException;
import com.identyum.appauth.AuthorizationService;
import com.identyum.demo.adapters.AddressesAdapter;
import com.identyum.demo.adapters.DocumentsAdapter;
import com.identyum.demo.adapters.PersonalNumbersAdapter;
import com.identyum.demo.adapters.model.Address;
import com.identyum.demo.adapters.model.Document;
import com.identyum.demo.adapters.model.PersonalNumber;
import com.identyum.demo.oauth.OAuth2Service;
import com.identyum.demo.rest.ApiClient;
import com.identyum.demo.rest.ApiInterface;
import com.identyum.demo.rest.model.ApiPersonalDataResponse;
import com.identyum.demo.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import lombok.val;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentPersonalData extends Fragment {

  private Context context;
  private AuthorizationService authorizationService;

  private RecyclerView addressesRecyclerView;
  private RecyclerView personalNumbersRecyclerView;
  private RecyclerView documentsRecyclerView;

  private AddressesAdapter addressesAdapter;
  private PersonalNumbersAdapter personalNumbersAdapter;
  private DocumentsAdapter documentsAdapter;

  private final List<Address> addresses = new ArrayList<>();
  private final List<PersonalNumber> personalNumbers = new ArrayList<>();
  private final List<Document> documents = new ArrayList<>();

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_personal_data, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getActivity().setTitle(R.string.personal_data_title);

    context = getContext();
    authorizationService = new AuthorizationService(context);

    /* ----- REFRESH OAUTH2 TOKEN IF NECESSARY & FETCH DATA ----- */

    OAuth2Service.authState.performActionWithFreshTokens(
        authorizationService,
        new AuthState.AuthStateAction() {
          @Override
          public void execute(String accessToken, AuthorizationException ex) {
            if (ex != null) {

              /* negotiation for fresh tokens failed, check ex for more details */
              Log.e(this.getClass().toString(), "Got exception while refreshing tokens");
              Log.e(this.getClass().toString(), Utils.generateStackTrace(ex));

              /* return to main/login activity via intent */
              OAuth2Service.logout(context);
              return;
            }

            /* use the access token to retrieve data */
            populateFragmentWithData(view, accessToken);
          }
        });
  }


  private void populateFragmentWithData(View view, String token) {
    val retrofit = ApiClient.getClient();
    val apiService = retrofit.create(ApiInterface.class);

    val call = apiService.getPersonalData("Bearer " + token, OAuth2Service.SECRET_KEY);
    call.enqueue(new Callback<ApiPersonalDataResponse>() {
      @Override
      public void onResponse(@NonNull Call<ApiPersonalDataResponse> call, @NonNull Response<ApiPersonalDataResponse> response) {
        Log.i(this.getClass().toString(), "ApiPersonalDataResponse retrieved");

        val body = response.body();
        if (body != null) {

          val firstName = body.getFirstName() == null ? "" : body.getFirstName().getValue();
          val lastName = body.getLastName() == null ? "" : body.getLastName().getValue();
          val dateOfBirth = body.getDateOfBirth() == null ? "" : body.getDateOfBirth().getValue();
          val nationalityCode = body.getNationalityCode() == null ? "" : body.getNationalityCode().getValue();
          val sex = body.getSex() == null ? "" : body.getSex().getValue();

          val firstNameView = (TextView) view.findViewById(R.id.personal_data_firstname_view);
          firstNameView.setText(firstName);

          val lastNameView = (TextView) view.findViewById(R.id.personal_data_lastname_view);
          lastNameView.setText(lastName);

          val dateOfBirthView = (TextView) view.findViewById(R.id.personal_data_date_of_birth_view);
          dateOfBirthView.setText(dateOfBirth);

          val nationalityCodeView = (TextView) view.findViewById(R.id.personal_data_nationality_code_view);
          nationalityCodeView.setText(nationalityCode);

          val sexView = (TextView) view.findViewById(R.id.personal_data_sex_view);
          sexView.setText(sex);

          addresses.clear();
          addresses.addAll(Address.from(body.getAddresses()));

          personalNumbers.clear();
          personalNumbers.addAll(PersonalNumber.from(body.getPersonalNumbers()));

          documents.clear();
          documents.addAll(Document.from(body.getDocuments()));

          addressesRecyclerView = view.findViewById(R.id.addresses_recycler_view);
          addressesRecyclerView.setLayoutManager(new LinearLayoutManager(context));
          addressesAdapter = new AddressesAdapter(addresses);
          addressesRecyclerView.setAdapter(addressesAdapter);

          personalNumbersRecyclerView = view.findViewById(R.id.personal_numbers_recycler_view);
          personalNumbersRecyclerView.setLayoutManager(new LinearLayoutManager(context));
          personalNumbersAdapter = new PersonalNumbersAdapter(personalNumbers);
          personalNumbersRecyclerView.setAdapter(personalNumbersAdapter);

          documentsRecyclerView = view.findViewById(R.id.documents_recycler_view);
          documentsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
          documentsAdapter = new DocumentsAdapter(documents);
          documentsRecyclerView.setAdapter(documentsAdapter);

          /* retrieving document images */
          for (int i = 0; i < documents.size(); i++) {
            val document = documents.get(i);

            val frontImageId = document.getFrontImage() == null ? null : document.getFrontImage().getId();
            val backImageId = document.getBackImage() == null ? null : document.getBackImage().getId();
            val signatureId = document.getSignatureImage() == null ? null : document.getSignatureImage().getId();

            if (frontImageId != null) {
              Utils.downloadDocumentImageAsync(
                  apiService,
                  documentsAdapter,
                  document,
                  token,
                  frontImageId,
                  Utils.TYPE_FRONT_IMAGE,
                  i
              );
            }

            if (backImageId != null) {
              Utils.downloadDocumentImageAsync(
                  apiService,
                  documentsAdapter,
                  document,
                  token,
                  backImageId,
                  Utils.TYPE_BACK_IMAGE,
                  i
              );
            }

            if (signatureId != null) {
              Utils.downloadDocumentImageAsync(
                  apiService,
                  documentsAdapter,
                  document,
                  token,
                  signatureId,
                  Utils.TYPE_SIGNATURE_IMAGE,
                  i
              );
            }
          }
        }
      }

      @Override
      public void onFailure(@NonNull Call<ApiPersonalDataResponse> call, @NonNull Throwable t) {
        Log.e(this.getClass().toString(), "Couldn't retrieve ApiPersonalDataResponse");
      }
    });
  }


  @Override
  public void onDestroy() {
    super.onDestroy();
    if (authorizationService != null) {
      authorizationService.dispose();
    }
  }

}
