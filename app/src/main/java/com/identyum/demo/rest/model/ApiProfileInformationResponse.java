package com.identyum.demo.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiProfileInformationResponse {

  private UUID userUuid;

  private List<ApiContact> phones;

  private List<ApiContact> emails;
}
