package com.identyum.demo.rest;

import com.identyum.demo.rest.model.ApiImageResponse;
import com.identyum.demo.rest.model.ApiLivenessImageResponse;
import com.identyum.demo.rest.model.ApiPersonalDataResponse;
import com.identyum.demo.rest.model.ApiProfileInformationResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface ApiInterface {

  @GET("profile")
  Call<ApiProfileInformationResponse> getProfileInformation(@Header("Authorization") String token);

  @GET("personal-data")
  Call<ApiPersonalDataResponse> getPersonalData(@Header("Authorization") String token, @Header("X-Secret-Key") String secretKey);

  @GET("liveness-check")
  Call<ApiLivenessImageResponse> getLivenessImage(@Header("Authorization") String token);

  @GET("image/{imageId}")
  Call<ApiImageResponse> getImage(@Path("imageId") String imageId, @Header("Authorization") String token, @Header("X-Secret-Key") String secretKey);

}

