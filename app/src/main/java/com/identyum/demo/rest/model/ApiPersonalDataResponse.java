package com.identyum.demo.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiPersonalDataResponse {

  private ApiValue firstName;

  private ApiValue lastName;

  private ApiValue fullName;

  private ApiValue nationalityCode;

  private ApiValue dateOfBirth;

  private ApiValue sex;

  private List<ApiValue> faceImages;

  private List<ApiAddress> addresses;

  private List<ApiPersonalNumber> personalNumbers;

  private List<ApiDocument> documents;

}
