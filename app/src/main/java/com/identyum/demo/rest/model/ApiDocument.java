package com.identyum.demo.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiDocument {

  private ApiValue frontImage;

  private ApiValue backImage;

  private ApiValue signatureImage;

  private ApiValue dateOfExpiry;

  private ApiValue dateOfIssue;

  private ApiValue type;

  private ApiValue issuingCountryCode;

  private ApiValue number;

  private ApiValue issuedBy;

}
