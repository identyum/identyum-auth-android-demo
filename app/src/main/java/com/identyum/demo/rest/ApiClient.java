package com.identyum.demo.rest;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ApiClient {

  /* BASE_URL must end with '/', otherwise FATAL EXCEPTION is thrown by Retrofit */
  public static final String BASE_URL = "https://identifier.stage.identyum.com/api/v1/identity/";

  public static Retrofit getClient() {
    return new Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(JacksonConverterFactory.create())
        .build();
  }
}

