package com.identyum.demo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.identyum.appauth.AuthState;
import com.identyum.appauth.AuthorizationException;
import com.identyum.appauth.AuthorizationService;
import com.identyum.demo.oauth.OAuth2Service;
import com.identyum.demo.rest.ApiClient;
import com.identyum.demo.rest.ApiInterface;
import com.identyum.demo.rest.model.ApiLivenessImageResponse;
import com.identyum.demo.utils.Utils;

import lombok.val;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentLivenessImage extends Fragment {

  private Context context;
  private AuthorizationService authorizationService;

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_liveness_image, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getActivity().setTitle(R.string.liveness_image_title);

    context = getContext();
    authorizationService = new AuthorizationService(context);

    /* ----- REFRESH OAUTH2 TOKEN IF NECESSARY & FETCH DATA ----- */

    OAuth2Service.authState.performActionWithFreshTokens(
        authorizationService,
        new AuthState.AuthStateAction() {
          @Override
          public void execute(String accessToken, AuthorizationException ex) {
            if (ex != null) {

              /* negotiation for fresh tokens failed, check ex for more details */
              Log.e(this.getClass().toString(), "Got exception while refreshing tokens");
              Log.e(this.getClass().toString(), Utils.generateStackTrace(ex));

              /* return to main/login activity via intent */
              OAuth2Service.logout(context);
              return;
            }

            /* use the access token to retrieve data */
            populateFragmentWithData(view, accessToken);
          }
        });
  }


  private void populateFragmentWithData(View view, String token) {
    val retrofit = ApiClient.getClient();
    val apiService = retrofit.create(ApiInterface.class);

    val call = apiService.getLivenessImage("Bearer " + token);
    call.enqueue(new Callback<ApiLivenessImageResponse>() {
      @Override
      public void onResponse(@NonNull Call<ApiLivenessImageResponse> call, @NonNull Response<ApiLivenessImageResponse> response) {
        Log.i(this.getClass().toString(), "ApiLivenessImageResponse retrieved");

        val body = response.body();
        if (body != null) {
          val decodedImageBytes = Base64.decode(body.getFaceImage(), Base64.DEFAULT);
            if (decodedImageBytes == null || decodedImageBytes.length == 0) {
                return;
            }

          val imageView = (ImageView) view.findViewById(R.id.face_image_view);
          imageView.setImageBitmap(
              BitmapFactory.decodeByteArray(
                  decodedImageBytes,
                  0,
                  decodedImageBytes.length
              )
          );
        }
      }

      @Override
      public void onFailure(@NonNull Call<ApiLivenessImageResponse> call, @NonNull Throwable t) {
        Log.e(this.getClass().toString(), "Couldn't retrieve ApiLivenessImageResponse");
      }
    });
  }


  @Override
  public void onDestroy() {
    super.onDestroy();
    if (authorizationService != null) {
      authorizationService.dispose();
    }
  }

}
