package com.identyum.demo;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.identyum.appauth.AuthState;
import com.identyum.appauth.AuthorizationRequest;
import com.identyum.appauth.AuthorizationService;
import com.identyum.appauth.AuthorizationServiceConfiguration;
import com.identyum.appauth.ResponseTypeValues;
import com.identyum.demo.oauth.OAuth2Service;

import lombok.val;

public class MainActivity extends AppCompatActivity {

  private AuthorizationService authorizationService;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    /* Restoring authState and going directly into
     * CoreDataHandlingActivity if user already logged-in
     */
    OAuth2Service.restoreState(this);
    if (OAuth2Service.authState != null && OAuth2Service.authState.isAuthorized()) {
      startActivity(new Intent(this, CoreDataHandlingActivity.class));
    }
  }

  /* ----- OAUTH2 AUTHORIZATION ----- */

  public void performLogin(View v) {

    Log.i("IdentyumDemoApp", "button clicked");

    val serviceConfig = AuthorizationServiceConfiguration.sandbox();
    OAuth2Service.authState = new AuthState(serviceConfig);

    val authRequestBuilder = new AuthorizationRequest.Builder(
        serviceConfig,
        OAuth2Service.CLIENT_ID,
        ResponseTypeValues.CODE,
        Uri.parse(OAuth2Service.REDIRECT_URI)
    );

    authRequestBuilder.setScope(OAuth2Service.SCOPES);

    /* In case specific browsers should be used for custom tabs
     * use the following to create AuthorizationService object.
     */
//   val appAuthConfig = new AppAuthConfiguration.Builder()
//          .setBrowserMatcher(new BrowserAllowList(
//              VersionedBrowserMatcher.CHROME_CUSTOM_TAB,
//              VersionedBrowserMatcher.SAMSUNG_CUSTOM_TAB))
//          .build();
//   authorizationService = new AuthorizationService(this, appAuthConfig);

    authorizationService = new AuthorizationService(this);
    authorizationService.performAuthorizationRequest(
        authRequestBuilder.build(),
        PendingIntent.getActivity(this, 0, new Intent(this, CoreDataHandlingActivity.class), PendingIntent.FLAG_MUTABLE),
        PendingIntent.getActivity(this, 0, new Intent(this, CoreDataHandlingActivity.class), PendingIntent.FLAG_MUTABLE)
    );
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (authorizationService != null) {
      authorizationService.dispose();
    }
  }

  @Override
  public void onBackPressed() {
  }

}