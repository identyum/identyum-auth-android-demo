package com.identyum.demo.oauth;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.identyum.appauth.AuthState;
import com.identyum.demo.MainActivity;

import org.json.JSONException;

import lombok.val;

public class OAuth2Service {

  private static final String AUTH_STATE_PREFERENCES_FILE = "oauth";
  private static final String AUTH_STATE_PERSISTENCE_KEY = "AUTH_STATE";

  public static final String REDIRECT_URI = "com.identyum.demo://oauth2Redirect";
  public static final String CLIENT_ID = "idy_mobile_demo_stage";
  public static final String SCOPES = "liveness_check id_scan";
  public static final String SECRET_KEY = "12345";

  public static AuthState authState = null;

  public static void restoreState(Context context) {
    val preferences = context.getSharedPreferences(AUTH_STATE_PREFERENCES_FILE, Context.MODE_PRIVATE);
    val authStateString = preferences.getString(AUTH_STATE_PERSISTENCE_KEY, null);
    if (authStateString != null) {
      try {
        OAuth2Service.authState = AuthState.jsonDeserialize(authStateString);
      } catch (JSONException e) {
        Log.e(OAuth2Service.class.toString(), "Could not deserialize auth state");
      }
    }
  }

  public static void saveState(Context context) {
    val preferences = context.getSharedPreferences(AUTH_STATE_PREFERENCES_FILE, Context.MODE_PRIVATE);
    val editor = preferences.edit();
    if (OAuth2Service.authState != null) {
      editor.putString(AUTH_STATE_PERSISTENCE_KEY, OAuth2Service.authState.jsonSerializeString());
      editor.commit();
    }
  }

  public static void clearState(Context context) {
    OAuth2Service.authState = null;
    val preferences = context.getSharedPreferences(AUTH_STATE_PREFERENCES_FILE, Context.MODE_PRIVATE);
    val editor = preferences.edit();
    editor.remove(AUTH_STATE_PERSISTENCE_KEY);
    editor.commit();
  }

  public static void logout(Context context) {
    OAuth2Service.clearState(context);
    val intent = new Intent(context, MainActivity.class);
    context.startActivity(intent);
  }
}
