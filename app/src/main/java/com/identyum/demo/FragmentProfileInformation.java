package com.identyum.demo;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.identyum.appauth.AuthState;
import com.identyum.appauth.AuthorizationException;
import com.identyum.appauth.AuthorizationService;
import com.identyum.demo.adapters.ProfileContactsAdapter;
import com.identyum.demo.adapters.model.ProfileContact;
import com.identyum.demo.oauth.OAuth2Service;
import com.identyum.demo.rest.ApiClient;
import com.identyum.demo.rest.ApiInterface;
import com.identyum.demo.rest.model.ApiProfileInformationResponse;
import com.identyum.demo.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import lombok.val;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentProfileInformation extends Fragment {

  private Context context;
  private AuthorizationService authorizationService;

  private RecyclerView profilePhonesRecyclerView;
  private RecyclerView profileEmailsRecyclerView;

  private ProfileContactsAdapter profilePhonesAdapter;
  private ProfileContactsAdapter profileEmailsAdapter;

  private final List<ProfileContact> phones = new ArrayList<>();
  private final List<ProfileContact> emails = new ArrayList<>();

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_profile_information, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getActivity().setTitle(R.string.profile_information_title);

    context = getContext();
    authorizationService = new AuthorizationService(context);

    /* ----- REFRESH OAUTH2 TOKEN IF NECESSARY & FETCH DATA ----- */

    OAuth2Service.authState.performActionWithFreshTokens(
        authorizationService,
        new AuthState.AuthStateAction() {
          @Override
          public void execute(String accessToken, AuthorizationException ex) {
            if (ex != null) {

              /* negotiation for fresh tokens failed, check ex for more details */
              Log.e(this.getClass().toString(), "Got exception while refreshing tokens");
              Log.e(this.getClass().toString(), Utils.generateStackTrace(ex));

              /* return to main/login activity via intent */
              OAuth2Service.logout(context);
              return;
            }

            /* use the access token to retrieve data */
            populateFragmentWithData(view, accessToken);
          }
        });
  }


  private void populateFragmentWithData(View view, String token) {
    val retrofit = ApiClient.getClient();
    val apiService = retrofit.create(ApiInterface.class);

    val call = apiService.getProfileInformation("Bearer " + token);
    call.enqueue(new Callback<ApiProfileInformationResponse>() {
      @Override
      public void onResponse(@NonNull Call<ApiProfileInformationResponse> call, @NonNull Response<ApiProfileInformationResponse> response) {
        Log.i(this.getClass().toString(), "ApiProfileInformationResponse retrieved");

        val body = response.body();
        if (body != null) {

          val userUuid = body.getUserUuid();
          if (userUuid != null) {
            val imageView = (TextView) view.findViewById(R.id.profile_user_uuid);
            imageView.setText(body.getUserUuid().toString());
          }

          phones.clear();
          phones.addAll(ProfileContact.from(body.getPhones()));

          emails.clear();
          emails.addAll(ProfileContact.from(body.getEmails()));

          profilePhonesRecyclerView = view.findViewById(R.id.profile_phones_recycler_view);
          profilePhonesRecyclerView.setLayoutManager(new LinearLayoutManager(context));

          profileEmailsRecyclerView = view.findViewById(R.id.profile_emails_recycler_view);
          profileEmailsRecyclerView.setLayoutManager(new LinearLayoutManager(context));

          profilePhonesAdapter = new ProfileContactsAdapter(phones);
          profilePhonesRecyclerView.setAdapter(profilePhonesAdapter);

          profileEmailsAdapter = new ProfileContactsAdapter(emails);
          profileEmailsRecyclerView.setAdapter(profileEmailsAdapter);
        }
      }

      @Override
      public void onFailure(@NonNull Call<ApiProfileInformationResponse> call, @NonNull Throwable t) {
        Log.e(this.getClass().toString(), "Couldn't retrieve ApiProfileInformationResponse");
      }
    });
  }


  @Override
  public void onDestroy() {
    super.onDestroy();
    if (authorizationService != null) {
      authorizationService.dispose();
    }
  }

}
